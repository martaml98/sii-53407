# Archivo Changelog.txt del proyecto
## Version [1.5] (27-12-20)
	-Añadimos comunicación local mediante sockets entre cliente y servidor.
	-Finalización de programa mediante el atributo acabar. 

## Version [1.4] (13-12-20)
	-Añadimos comunicación cliente servidor a través de dos tuberías con 		nombre
	-Incluimos hilo que espere a que el cliente pulse una tecla

## Version [1.3] (23-11-20)
	-Incluimos logger que nos indica los puntos que mete cada jugador
	-Incluimos bot para controlar la raqueta1 de forma automática

	
## Version [1.2] (29-10-2020) 
	-Damos movimiento a la esfera y a las raquetas.
	-La pelota disminuye de tamaño según avanza el juego, hasta que llega a 	un mínimo y vuelve a su tamaño original.

## Version [1.1] (15-10-2020)
	-Añadimos archivo Changelog.md
	
