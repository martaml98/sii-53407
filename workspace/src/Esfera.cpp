// Esfera.cpp: implementation of the Esfera class.
//Autora: Marta Muñoz Lázaro
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	//Hay que darle movimiento a la esfera
	centro.x=centro.x+velocidad.x*t;
	centro.y=centro.y+velocidad.y*t;
}

void Esfera::CambiaTamano(float t){
	radio-=radio*t;
	if(radio<=0.15) radio=0.5;
}
