#include <iostream>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "Puntuaciones.h"

int main (){
	int fd;
	int ret;
	Prueba prueba;

	//Creamos FIFO
	ret=mkfifo("tub",0666);
	if(ret<0){
		perror("mkfifo");
		return 1;
	}
	//Abrimos FIFO
	fd=open("tub",O_RDONLY);
	if(fd<0){
                perror("open");
                return 1;
        }
	while((read(fd,&prueba,sizeof(prueba)))!=0){
		if(prueba.heMarcado==1)
			printf("Jugador %d marca %d puntos\n",prueba.heMarcado,prueba.jugador1);
		else
			printf("Jugador %d marca %d puntos\n",prueba.heMarcado,prueba.jugador2);

	}
	
	close(fd);
	unlink("tub");
return 0;
}

