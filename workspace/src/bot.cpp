#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdio.h>
#include <unistd.h>

#include "DatosMemCompartida.h"

int main (){
	int fd; //Para abrir el fichero proyectado
	DatosMemCompartida *punt;
	fd = open("/tmp/mifich",O_RDWR);
	if(fd<0){
		perror("Error al abrir el fichero proyectado");
		close(fd);
		return 1;
	}
	punt=static_cast<DatosMemCompartida*>(mmap(NULL,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,fd,0));
	if(punt==MAP_FAILED){
		perror("Error al proyectar en bot");
		close(fd);
		return 1;
	}
	close(fd);
	
	while(1){
		float PosRaqueta = (punt->raqueta1.y1 + punt->raqueta1.y2)/2;

		if(PosRaqueta < punt->esfera.centro.y)
			punt->accion=1;
		else if(PosRaqueta == punt->esfera.centro.y)
			punt->accion=0;
		else
			punt->accion=-1;
		usleep(1000);
	}
	munmap(punt,sizeof(DatosMemCompartida));
	unlink("/temp/mifich");

return 0;
}
