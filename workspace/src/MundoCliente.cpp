// Mundo.cpp: implementation of the CMundo class.
//Autora: Marta Muñoz Lázaro
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{
	munmap(p,sizeof(datos));
	unlink("/tmp/mifich");

	/*close(fd_4);
	unlink("tub2");

	close(fd_cliente_servidor);
	unlink("tub3");*/
}


void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{
	
	//Práctica 4
        char cad[200];
        //read(fd_4,cad,sizeof(cad));
	s_comunicacion.Receive(cad,sizeof(cad));
        sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d",&esfera.centro.x,&esfera.centro.y,&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2,&puntos1,&puntos2);



	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
        esfera.CambiaTamano(0.005f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
        
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
	}
	
	p->esfera=esfera;
	p->raqueta1=jugador1;
	
	if(p->accion==-1) {OnKeyboardDown('s',0,0);}
	else if(p->accion==0){}
	else if(p->accion==1) {OnKeyboardDown('w',0,0);}

}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	char tecla[]="0";
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':sprintf(tecla,"s");break;
	case 'w':sprintf(tecla,"w");break;
	case 'l':sprintf(tecla,"l");break;
	case 'o':sprintf(tecla,"o");break;
	}

	/* Practica4
	write(fd_cliente_servidor,&key,sizeof(key));
	*/
	s_comunicacion.Send(tecla,sizeof(tecla));
}

void CMundoCliente::Init()
{
	/*
	//Práctica 4
	mkfifo("tub2",0666);
	fd_4=open("tub2",O_RDONLY);
	if(fd_4<0){
		perror("open tubería2");
		return;
	}

	mkfifo("tub3",0666);
	fd_cliente_servidor=open("tub3",O_WRONLY);
	if(fd_cliente_servidor<0){
                perror("open tubería3");
                return;
        }
	//Fin práctica 4
	*/

	//Para practica 5
	char ip[]="127.0.0.1";
	char nombre[50];
	printf("Escriba su nombre: ");
	scanf("%s",nombre);
	s_comunicacion.Connect(ip,8000);
	//Enviamos el nombre
	s_comunicacion.Send(nombre,sizeof(nombre));

	//Proyectar en memoria fichero -- PRÁCTICA 3
	int fd_fichero;
	//Crear un fichero del tamaño de atributo DatosMemCompartida
        if((fd_fichero=open("/tmp/mifich",O_CREAT|O_TRUNC|O_RDWR,0666))<0){
                perror("open");
                return;
        }
        datos.esfera=esfera;
        datos.raqueta1=jugador1;
        datos.accion=0;
        write(fd_fichero,&datos,sizeof(datos));
        //Forzamos a que lo que devuelva sea un puntero a DatosMemCompartida
        p=static_cast<DatosMemCompartida*>(mmap(NULL,sizeof(datos),PROT_READ|PROT_WRITE,MAP_SHARED,fd_fichero,0));
	if(p==MAP_FAILED){
                perror("mmap");
                close(fd_fichero);
		return;
        }

        close(fd_fichero);

////////////////////////////////////////////////


	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
