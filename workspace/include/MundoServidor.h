// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "Puntuaciones.h"
#include "Socket.h"

#include "DatosMemCompartida.h"

class CMundoServidor
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	void RecibeComandosJugador(); //Practica4!!!!!!!!1

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int fd;//Descriptor de fichero para el FIFO

	Prueba prueba; //Para las puntuaciones pasadas por tubería	


	int puntos1;
	int puntos2;

	DatosMemCompartida datos; //Añadido práctica 3
	DatosMemCompartida *p; //Añadido práctica 3
	/*
	//Para praćtica 4
	int fd_4;
	int fd_cliente_servidor;
	*/

	pthread_t thid1; //Identificador hilo
	pthread_attr_t atrib;
	//Practica 5
	//Declaramos dos sockets uno para la conexión y otro para la comunicación
	Socket s_comunicacion;
	Socket s_conexion;
	int acabar;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
